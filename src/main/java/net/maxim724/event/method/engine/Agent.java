package net.maxim724.event.method.engine;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;

/**
 * Посредник при вызове подписчиков, зарегистрированных с помощью {@link net.maxim724.event.api.method.Subscribe}
 *
 * @param <L> тип слушателя
 * @param <E> тип события
 *
 * @since 724.0
 */
@FunctionalInterface
public interface Agent<L, E> {

  /**
   * Вызвать обработку события
   * @param listener слушатель
   * @param event события
   * @throws Throwable любая хрень при обработке
   */
  void invoke(@NotNull L listener, @NotNull E event) throws Throwable;

  /**
   * Фабрика посредников
   * @param <L> тип слушателя
   * @param <E> тип события
   */
  @FunctionalInterface
  interface Factory<L, E> {
    @NotNull Agent<L, E> create(@NotNull Object object, @NotNull Method method) throws Exception;
  }
}