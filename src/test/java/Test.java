import net.maxim724.event.api.Priority;
import net.maxim724.event.api.method.MethodEventProvider;
import net.maxim724.event.api.method.Subscribe;
import net.maxim724.event.method.DefaultMethodEventProvider;

public final class Test {

    public static void main(String... args) {
        MethodEventProvider<Object, Object> provider = DefaultMethodEventProvider.COMMON_EVENT_PROVIDER;

        Listener listener = new Listener();
        provider.register(listener);

        Listener listener2 = new Listener();
        provider.register(listener2);

        provider.publish(new TestEvent());

        provider.unregister(listener);
        System.out.println("unreg");

        provider.publish(new TestEvent());
    }

    public static class Listener {
        @Subscribe(priority = Priority.NORMAL)
        public void listen(TestEvent event) {
            System.out.println("Consumed!");
        }
    }

    public static class TestEvent {
        public int lol = 5;
    }
}